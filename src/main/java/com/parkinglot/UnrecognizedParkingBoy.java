package com.parkinglot;

public class UnrecognizedParkingBoy extends RuntimeException{
    public UnrecognizedParkingBoy() {
        super("No ParkingLotManager's ParkingBoy");
    }

}
