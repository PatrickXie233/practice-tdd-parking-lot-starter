package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingBoyByParkingLot implements ParkingBoy{
    private ArrayList<ParkingLot> parkingLotArrayList = new ArrayList<ParkingLot>();

    public ParkingBoyByParkingLot(ArrayList<ParkingLot> parkingLotArrayList) {
        this.parkingLotArrayList = parkingLotArrayList;
    }

    public ParkingBoyByParkingLot() {
    }

    public Ticket parking(Car car) {
        return parkingLotArrayList.stream()
                .filter(ParkingLot::havePosition)
                .findFirst()
                .map(parkingLot -> parkingLot.parking(car))
                .orElseThrow(UnavailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        ParkingLot matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket))
                .findFirst()
                .orElse(null);
        if (matchParkingLotList == null) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.fetch(ticket);
    }

    public ParkingLot findParkingLotByTicket(Ticket ticket) {
        List<ParkingLot> matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket)).collect(Collectors.toList());
        if (matchParkingLotList.size() == 0) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.get(0);
    }
    @Override
    public boolean haveEmptyPosition(){
        return parkingLotArrayList.stream().noneMatch(ParkingLot::havePosition);
    }
}
