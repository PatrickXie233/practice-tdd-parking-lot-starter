package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<Ticket, Car> carList = new HashMap<>();
    private int parkingLotNumber = 4;

    public ParkingLot(int parkingLotNumber) {
        this.parkingLotNumber = parkingLotNumber;
    }

    public Ticket parking(Car car) {
        if (carList.size() == parkingLotNumber) {
            throw new UnavailablePositionException();
        }
        Ticket ticket = new Ticket();
        carList.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!matchCarByTicket(ticket)) {
            throw new UnrecognizedTicketExcepton();
        }
        Car car = carList.get(ticket);
        carList.remove(ticket);
        return car;
    }

    public Map<Ticket, Car> getCarList() {
        return carList;
    }

    public int getParkingLotNumber() {
        return parkingLotNumber;
    }

    public boolean matchCarByTicket(Ticket ticket) {
        return carList.containsKey(ticket);
    }

    public boolean havePosition() {
        return carList.size() < parkingLotNumber;
    }
    public int getNumberOfEmptyPosition(){
        return parkingLotNumber - carList.size();
    }
    public float getEmptyPositionRate(){
        System.out.println((float) carList.size());
        System.out.println(parkingLotNumber);
        System.out.println(1 - (float) carList.size() / parkingLotNumber);

        return 1 - (float) carList.size() / parkingLotNumber;
    }

}
