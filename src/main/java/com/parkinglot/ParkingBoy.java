package com.parkinglot;

import java.util.ArrayList;

public interface ParkingBoy {
    ArrayList<ParkingLot> parkingLotArrayList = new ArrayList<>();
    Ticket parking(Car car);
    Car fetch(Ticket ticket);

    boolean haveEmptyPosition();
}
