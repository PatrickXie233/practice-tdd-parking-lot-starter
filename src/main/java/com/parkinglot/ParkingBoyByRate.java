package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingBoyByRate implements ParkingBoy{
    private ArrayList<ParkingLot> parkingLotArrayList = new ArrayList<ParkingLot>();

    public ParkingBoyByRate(ArrayList<ParkingLot> parkingLotArrayList) {
        this.parkingLotArrayList = parkingLotArrayList;
    }

    public ParkingBoyByRate() {
    }

    @Override
    public Ticket parking(Car car) {
        ParkingLot moreEmptyPositionsRateParkingLot = getMoreEmptyPositionsRateParkingLot();

        if (moreEmptyPositionsRateParkingLot==null){
            throw new UnavailablePositionException();
        }

        return moreEmptyPositionsRateParkingLot.parking(car);
    }

    private ParkingLot getMoreEmptyPositionsRateParkingLot() {
        return parkingLotArrayList.stream()
                .max(Comparator.comparing(ParkingLot::getEmptyPositionRate))
                .orElse(null);
    }

    @Override
    public Car fetch(Ticket ticket) {
        ParkingLot matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket))
                .findFirst()
                .orElse(null);
        if (matchParkingLotList == null) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.fetch(ticket);
    }
    public ParkingLot findParkingLotByTicket(Ticket ticket) {
        List<ParkingLot> matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket)).collect(Collectors.toList());
        if (matchParkingLotList.size() == 0) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.get(0);
    }
    @Override
    public boolean haveEmptyPosition(){
        return parkingLotArrayList.stream().noneMatch(ParkingLot::havePosition);
    }
}
