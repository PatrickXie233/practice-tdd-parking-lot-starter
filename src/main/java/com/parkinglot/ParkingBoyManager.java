package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoyManager implements ParkingBoy{

    private List<ParkingBoy>  parkingBoyList = new ArrayList<>();


    @Override
    public Ticket parking(Car car) {
        return null;
    }

    public List<ParkingBoy> getParkingBoyList() {
        return parkingBoyList;
    }

    public void addParkingBoy(ParkingBoy parkingBoy){
        parkingBoyList.add(parkingBoy);
    }

    @Override
    public Car fetch(Ticket ticket) {
        ParkingLot matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket))
                .findFirst()
                .orElse(null);
        if (matchParkingLotList == null) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.fetch(ticket);
    }

    @Override
    public boolean haveEmptyPosition() {
        return parkingBoyList.stream()
                .filter(ParkingBoy::haveEmptyPosition)
                .findFirst()
                .orElse(null)!=null;
    }

    public Ticket assignParking(ParkingBoy parkingBoy,Car car) {
        if (!parkingBoyList.contains(parkingBoy)){
            throw new UnrecognizedParkingBoy();
        }
        return parkingBoy.parking(car);
    }
    public ArrayList<ParkingLot> getPakringLot(){
        return parkingLotArrayList;
    }

    public Car assignFetch(ParkingBoy parkingBoy1,Ticket ticket) {
        return parkingBoy1.fetch(ticket);
    }
}
