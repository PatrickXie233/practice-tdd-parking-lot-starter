package com.parkinglot;

public class UnrecognizedTicketExcepton extends RuntimeException{
    public UnrecognizedTicketExcepton(){
        super("Unrecognized parking ticket");
    }
}
