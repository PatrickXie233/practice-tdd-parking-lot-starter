package com.parkinglot;

public class UnavailablePositionException extends RuntimeException{
    public UnavailablePositionException() {
        super("No available position");
    }

}
