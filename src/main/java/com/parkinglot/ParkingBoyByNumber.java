package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingBoyByNumber implements ParkingBoy {
    private ArrayList<ParkingLot> parkingLotArrayList = new ArrayList<ParkingLot>();

    public ParkingBoyByNumber(ArrayList<ParkingLot> parkingLotArrayList) {
        this.parkingLotArrayList = parkingLotArrayList;
    }

    public void setParkingLotArrayList(ArrayList<ParkingLot> parkingLotArrayList) {
        this.parkingLotArrayList = parkingLotArrayList;
    }

    public ParkingBoyByNumber() {
    }

    public Ticket parking(Car car) {
        ParkingLot moreEmptyPositionsParkingLot = getMoreEmptyPositionsParkingLot();

        if (moreEmptyPositionsParkingLot==null){
            throw new UnavailablePositionException();
        }

        return moreEmptyPositionsParkingLot.parking(car);
    }

    public Car fetch(Ticket ticket) {
        ParkingLot matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket))
                .findFirst()
                .orElse(null);
        if (matchParkingLotList == null) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.fetch(ticket);
    }

    public ParkingLot findParkingLotByTicket(Ticket ticket) {
        List<ParkingLot> matchParkingLotList = parkingLotArrayList.stream()
                .filter(parkingLot -> parkingLot.matchCarByTicket(ticket)).collect(Collectors.toList());
        if (matchParkingLotList.size() == 0) {
            throw new UnrecognizedTicketExcepton();
        }
        return matchParkingLotList.get(0);
    }
    public ParkingLot getMoreEmptyPositionsParkingLot(){
        return parkingLotArrayList.stream()
                .max(Comparator.comparing(ParkingLot::getNumberOfEmptyPosition))
                .orElse(null);
    }
    @Override
    public boolean haveEmptyPosition(){
        return parkingLotArrayList.stream().noneMatch(ParkingLot::havePosition);
    }
}
