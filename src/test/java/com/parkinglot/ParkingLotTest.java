package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_a_car() {
//      give
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
//      when
        Ticket ticket = parkingLot.parking(car);
//      then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_given_ticket() {
//      given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket ticket = parkingLot.parking(car);
//      when
        Car fetchCar = parkingLot.fetch(ticket);
//      then
        Assertions.assertSame(car, fetchCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_two_ticket() {
//      given
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingLot parkingLot = new ParkingLot(4);
        Ticket ticket1 = parkingLot.parking(car1);
        Ticket ticket2 = parkingLot.parking(car2);
//      when
        Car returnCar1 = parkingLot.fetch(ticket1);
        Car returnCar2 = parkingLot.fetch(ticket2);

//      then
        Assertions.assertEquals(returnCar1, car1);
        Assertions.assertEquals(returnCar2, car2);
    }

    @Test
    void should_return_Unrecognized_parking_ticket_when_fetch_given_a_parking_lot_and_a_wrong_ticket() {
//      given
        ParkingLot parkingLot = new ParkingLot(4);

//      when then
        UnrecognizedTicketExcepton unrecognizedTicketExcepton
                = assertThrows(UnrecognizedTicketExcepton.class, () -> parkingLot.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketExcepton.getMessage());
    }

    @Test
    void should_return_Unrecognized_parking_ticket_when_fetch_given_a_used_ticket() {
//      given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket ticket = parkingLot.parking(car);
        parkingLot.fetch(ticket);
//      when
        UnrecognizedTicketExcepton unrecognizedTicketExcepton =
                assertThrows(UnrecognizedTicketExcepton.class, () -> parkingLot.fetch(ticket));
//      then
        assertEquals("Unrecognized parking ticket", unrecognizedTicketExcepton.getMessage());
    }

    @Test
    void should_return_No_available_position_when_park_a_car_given_parking_without_any_position() {
//      given
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();

        ParkingLot parkingLot = new ParkingLot(4);
        Ticket ticket1 = parkingLot.parking(car1);
        Ticket ticket2 = parkingLot.parking(car2);
        Ticket ticket3 = parkingLot.parking(car3);
        Ticket ticket4 = parkingLot.parking(car4);
//      when then
        UnavailablePositionException unavailablePositionException =
                assertThrows(UnavailablePositionException.class, () -> parkingLot.parking(car5));
        assertEquals("No available position", unavailablePositionException.getMessage());
    }

}
