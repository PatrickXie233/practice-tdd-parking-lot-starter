package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_boy_a_car() {
//      given
        Car car = new Car();
        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);
//      when
        Ticket ticket = parkingBoyByParkingLot.parking(car);
//      then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_given_parking_boy_ticket() {
//      given
        Car car = new Car();
        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);
        Ticket ticket = parkingBoyByParkingLot.parking(car);
//      when
        Car fetchCar = parkingBoyByParkingLot.fetch(ticket);
//      then
        Assertions.assertSame(car, fetchCar);
    }
//
    @Test
    void should_return_right_car_when_fetch_given_parking_boy_two_ticket() {
        //      given
        Car car1 = new Car();
        Car car2 = new Car();
        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);
        Ticket ticket1 = parkingBoyByParkingLot.parking(car1);
        Ticket ticket2 = parkingBoyByParkingLot.parking(car2);
//      when
        Car returnCar1 = parkingBoyByParkingLot.fetch(ticket1);
        Car returnCar2 = parkingBoyByParkingLot.fetch(ticket2);

//      then
        Assertions.assertEquals(returnCar1, car1);
        Assertions.assertEquals(returnCar2, car2);
    }
//
    @Test
    void should_return_Unrecognized_parking_ticket_when_fetch_given_parking_boy_and_a_wrong_ticket() {
//      given
        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);

//      when then
        UnrecognizedTicketExcepton unrecognizedTicketExcepton
                = assertThrows(UnrecognizedTicketExcepton.class, () -> parkingBoyByParkingLot.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket", unrecognizedTicketExcepton.getMessage());
    }
//
    @Test
    void should_return_Unrecognized_parking_ticket_when_fetch_given_parking_boy_a_used_ticket() {
//      given
        Car car = new Car();
        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);
        Ticket ticket = parkingBoyByParkingLot.parking(car);
        parkingBoyByParkingLot.fetch(ticket);
//      when
        UnrecognizedTicketExcepton unrecognizedTicketExcepton =
                assertThrows(UnrecognizedTicketExcepton.class, () -> parkingBoyByParkingLot.fetch(ticket));
//      then
        assertEquals("Unrecognized parking ticket", unrecognizedTicketExcepton.getMessage());
    }
//
    @Test
    void should_return_No_available_position_when_give_parking_boy_park_a_car_given_parking_without_any_position() {
//      given
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();
        Car car6 = new Car();
        Car car7 = new Car();
        Car car8 = new Car();
        Car car9 = new Car();

        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);

        parkingBoyByParkingLot.parking(car1);
        parkingBoyByParkingLot.parking(car2);
        parkingBoyByParkingLot.parking(car3);
        parkingBoyByParkingLot.parking(car4);
        parkingBoyByParkingLot.parking(car5);
        parkingBoyByParkingLot.parking(car6);
        parkingBoyByParkingLot.parking(car7);
        parkingBoyByParkingLot.parking(car8);

//      when then
        UnavailablePositionException unavailablePositionException =
                assertThrows(UnavailablePositionException.class, () -> parkingBoyByParkingLot.parking(car9));
        assertEquals("No available position", unavailablePositionException.getMessage());
    }

    @Test
    void should_parking_second_parking_lot_when_the_first_parking_lot_full_but_second_parking_lot_have_position_given_parking_boy_parking() {
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(4);
        ParkingLot parkingLot2 = new ParkingLot(4);

        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLot1.parking(car1);
        parkingLot1.parking(car2);
        parkingLot1.parking(car3);
        parkingLot1.parking(car4);

        parkingLotArrayList.add(parkingLot1);
        parkingLotArrayList.add(parkingLot2);
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);

        Ticket ticket5 = parkingBoyByParkingLot.parking(car5);

        assertSame(parkingLot2, parkingBoyByParkingLot.findParkingLotByTicket(ticket5));
    }

    @Test
    void should_return_two_right_car_when_fetch_the_car_twice_given_parking_boy_two_ticket() {
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();
        Car car6 = new Car();
        Car car7 = new Car();
        Car car8 = new Car();

        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        parkingLotArrayList.add(new ParkingLot(4));
        parkingLotArrayList.add(new ParkingLot(4));
        ParkingBoyByParkingLot parkingBoyByParkingLot = new ParkingBoyByParkingLot(parkingLotArrayList);

        parkingBoyByParkingLot.parking(car1);
        parkingBoyByParkingLot.parking(car2);
        Ticket ticket3 = parkingBoyByParkingLot.parking(car3);
        parkingBoyByParkingLot.parking(car4);
        parkingBoyByParkingLot.parking(car5);
        parkingBoyByParkingLot.parking(car6);
        Ticket ticket7 = parkingBoyByParkingLot.parking(car7);
        parkingBoyByParkingLot.parking(car8);

        assertSame(parkingBoyByParkingLot.fetch(ticket3), car3);
        assertSame(parkingBoyByParkingLot.fetch(ticket7), car7);
    }

    @Test
    void should_parking_more_empty_parking_lot_when_parking_boy_parking_car_given_a_car() {
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();
        Car car6 = new Car();

        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        ParkingLot parkingLot1 = new ParkingLot(4);//1
        ParkingLot parkingLot2 = new ParkingLot(5);//4
        parkingLot1.parking(car1);
        parkingLot1.parking(car2);
        parkingLot1.parking(car3);
        parkingLot2.parking(car4);
        parkingLot2.parking(car5);

        parkingLotArrayList.add(parkingLot1);
        parkingLotArrayList.add(parkingLot2);

        ParkingBoyByNumber parkingBoyByNumber = new ParkingBoyByNumber(parkingLotArrayList);

        Ticket ticket = parkingBoyByNumber.parking(car6);

        assertSame(parkingLot2, parkingBoyByNumber.findParkingLotByTicket(ticket));
    }

    @Test
    void should_parking_larger_available_position_rate_parking_lot_when_parking_boy_parking_car_given_a_car() {
//      given
        Car car = new Car();

        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(7);
        for (int i = 0; i < 3; i++) {
            parkingLot1.parking(new Car());
        }
        for (int i = 0; i < 4; i++) {
            parkingLot2.parking(new Car());
        }
        parkingLotArrayList.add(parkingLot1);
        parkingLotArrayList.add(parkingLot2);
        ParkingBoyByRate parkingBoyByRate = new ParkingBoyByRate(parkingLotArrayList);
//      when

        Ticket ticket = parkingBoyByRate.parking(car);

        assertSame(parkingLot2, parkingBoyByRate.findParkingLotByTicket(ticket));
    }
    //

    @Test
    void should_add_parkingBoy_in_management_list_when_addParkingBoy_given_parking_boy () {
        ParkingBoyManager parkingBoyManager = new ParkingBoyManager();
        ParkingBoy parkingBoy1 = new ParkingBoyByNumber();
        ParkingBoy parkingBoy2 = new ParkingBoyByParkingLot();
        ParkingBoy parkingBoy3 = new ParkingBoyByRate();

        parkingBoyManager.addParkingBoy(parkingBoy1);
        parkingBoyManager.addParkingBoy(parkingBoy2);
        parkingBoyManager.addParkingBoy(parkingBoy3);

        List<ParkingBoy> parkingBoyList = parkingBoyManager.getParkingBoyList();
        boolean isContained = parkingBoyList.containsAll(Arrays.asList(parkingBoy1, parkingBoy2, parkingBoy3));
        Assertions.assertTrue(isContained);
    }

    @Test
    void should_assign_a_parking_boy_parking_when_assignParking_given_parking_boy() {
        ParkingBoyManager parkingBoyManager = new ParkingBoyManager();
        ArrayList<ParkingLot>  parkingLotArrayList = new ArrayList<ParkingLot>();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(7);
        parkingLotArrayList.add(parkingLot1);
        parkingLotArrayList.add(parkingLot2);

        ParkingBoy parkingBoy1 = new ParkingBoyByNumber(parkingLotArrayList);
        ParkingBoy parkingBoy2 = new ParkingBoyByParkingLot();
        ParkingBoy parkingBoy3 = new ParkingBoyByRate();

        parkingBoyManager.addParkingBoy(parkingBoy1);
        parkingBoyManager.addParkingBoy(parkingBoy2);
        parkingBoyManager.addParkingBoy(parkingBoy3);
        Car car = new Car();
        Ticket ticket = parkingBoyManager.assignParking(parkingBoy1,car);
        Car car1 = parkingBoyManager.assignFetch(parkingBoy1,ticket);
        assertSame(car1, car);
    }
}
