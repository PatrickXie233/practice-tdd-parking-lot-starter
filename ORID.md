**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview:我代码中存在许多问题，比如：方法名打错，代码冗余，commit type后应加空格，方法名应该用驼峰式
2. OOP与TDD实践：从最简单的parking lot的需求分析到拆分task case，再到编写测试代码，最后实现并重构，在老师的引导下逐渐增加需求，对现有的代码进行重构，实践了 TDD开发流程。

**R (Reflective): Please use one word to express your feelings about today's class.**

实践完整流程

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

在编写代码时，要多考虑代码的可读性，比如我今天用get（0）获取数组中的第一个数导致其他人在阅读代码时可能存在误解。

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

我们组周末要用策略模式做team work，但是我还没学习策略模式，在开始前要多去学习策略模式